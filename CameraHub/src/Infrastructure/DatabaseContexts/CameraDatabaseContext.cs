﻿using Infrastructure.DatabaseContexts.Configuration;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.DatabaseContexts
{
    public class CameraDatabaseContext : DbContext
    {
        public CameraDatabaseContext(DbContextOptions options)
            : base(options)
        {
        }

        public DbContext GetDbContext()
        {
            return this;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("camera_hub");

            modelBuilder.ApplyConfiguration(new CameraEntityTypeConfiguration());                               
        }
    }
}
