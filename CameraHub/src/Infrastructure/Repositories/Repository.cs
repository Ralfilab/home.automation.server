﻿using Infrastructure.DatabaseContexts;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;
using SharedKernel;
using SharedKernel.Interfaces;

namespace Infrastructure.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity>
        where TEntity : Entity
    {
        protected readonly DbContext dbContext;
        protected readonly DbSet<TEntity> entities;

        public Repository(CameraDatabaseContext context)
        {
            this.dbContext = context;            
            this.entities = this.dbContext.Set<TEntity>();            
        }       

        public TEntity Get(long id)
        {
            return entities.Find(id);
        }

        public TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate)
        {            
            return entities.FirstOrDefault(predicate);
        }

        public async Task<IEnumerable<TEntity>> ListReadOnlyAsync(IPaging paging)
        {
            throw new NotImplementedException();
        }

        public void Update(TEntity entity)
        {
            AttachIfNot(entity);

            dbContext.Entry(entity).State = EntityState.Modified;            
        }

        public TEntity Add(TEntity entity)
        {
            AttachIfNot(entity);

            dbContext.Entry(entity).State = EntityState.Added;

            return entities.Add(entity).Entity;
        }

        public async Task<TEntity> AddAsync(TEntity entity)
        {
            AttachIfNot(entity);

            dbContext.Entry(entity).State = EntityState.Added;

            var result = await entities.AddAsync(entity);

            return result.Entity;
        }

        public async Task<int> CountAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await entities.Where(predicate).CountAsync();
        }

        public async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            var result = await dbContext.SaveChangesAsync(cancellationToken).ConfigureAwait(false);

            return result;
        }

        protected virtual void AttachIfNot(TEntity entity)
        {
            if (entities.Local.Contains(entity) == false)
            {
                entities.Attach(entity);
            }
        }
    }
}
