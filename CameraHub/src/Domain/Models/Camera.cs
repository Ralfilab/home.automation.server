﻿using SharedKernel;

namespace Domain.Models
{
    public class Camera : AggregateRoot
    {
        public Camera(long id, string apiKey) : base(id)
        { 
            ApiKey = apiKey;
        }

        public bool MotionDetection { get; private set; }

        public string ApiKey { get; protected set; }

        public bool IsValid(string apiKey)
        { 
            return ApiKey == apiKey;
        }

        public void EnableMotionDetection()
        {
            MotionDetection = true;            
        }

        public void DisableMotionDetection()
        {
            MotionDetection = false;            
        }
    }
}