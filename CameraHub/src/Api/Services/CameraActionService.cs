﻿using Grpc.Core;
using Microsoft.AspNetCore.Authorization;
using System.Collections.Concurrent;
using UseCases.Cameras.Actions;
using UseCases.Cameras.Actions.Dtos;
using UseCases.Cameras.Status;
using UseCases.Files.UploadCase;

namespace CameraHub.Services
{
    public class CameraActionService : 
        CameraAction.CameraActionBase
    {
        private readonly CameraMotionDetectionEvent detectionMediator;
        private readonly CameraStatusCheckUseCase statusCheckUseCase;
        private readonly BlockingCollection<CameraMotionDetectionResponse> motionDetectionChange = new();
        private readonly FileUploadForCamera fileUploadForCamera;

        public CameraActionService(CameraMotionDetectionEvent detectionMediator,
            CameraStatusCheckUseCase statusCheckUseCase,
            FileUploadForCamera fileUploadForCamera)
        {
            this.detectionMediator = detectionMediator;
            this.statusCheckUseCase = statusCheckUseCase;
            this.fileUploadForCamera = fileUploadForCamera;
        }

        [Authorize]
        public override async Task SubscribeMotionDetection(MotionDetectionRequest request, IServerStreamWriter<MotionDetectionResponse> responseStream, ServerCallContext context)
        {
            var initialStatus = statusCheckUseCase.GetByApiKey(request.ApiKey);

            await responseStream.WriteAsync(new MotionDetectionResponse()
            {
                MotionDetectionEnabled = initialStatus
            });            

            detectionMediator.Updated += MotionStatusChange;

            while (!context.CancellationToken.IsCancellationRequested)
            {
                if (motionDetectionChange.TryTake(out var motionDetection) == false) 
                {
                    continue;
                }

                var isStatusCheckForCurrentCamera = request.ApiKey == motionDetection.ApiKey;
                
                if (isStatusCheckForCurrentCamera == false) 
                {
                    continue;
                }

                await responseStream.WriteAsync(new MotionDetectionResponse()
                {
                    MotionDetectionEnabled = motionDetection.Status
                });                
            }

            detectionMediator.Updated -= MotionStatusChange;
        }

        [Authorize]
        public override async Task<FileUploadResponse> FileUpload(IAsyncStreamReader<FileUploadRequest> requestStream, ServerCallContext context)
        {
            string apiKey = null;
            string fileExtension = null;

            IEnumerable<byte> bytes = Array.Empty<byte>();

            while (await requestStream.MoveNext())
            {
                if (requestStream.Current.Metadata != null)
                {
                    apiKey = requestStream.Current.Metadata.ApiKey;
                    fileExtension = requestStream.Current.Metadata.Extension;

                    continue;
                }

                bytes = bytes.Concat(requestStream.Current.ChunkData);                
            }

            await fileUploadForCamera.Add(new UseCases.Files.UploadCase.Dtos.FileUploadRequest()
            {
                CameraApiKey = apiKey,
                FileExtension = fileExtension,
                FileBytes = bytes.ToArray()
            });

            return new FileUploadResponse()
            { 
                Success = true
            };            
        }

        internal void MotionStatusChange(object? sender, CameraMotionDetectionResponse response) 
            => motionDetectionChange.TryAdd(response);
    }
}
