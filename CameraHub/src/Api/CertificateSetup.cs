﻿using Microsoft.AspNetCore.Server.Kestrel.Core;
using System.Security.Cryptography.X509Certificates;

namespace Api
{
    public static class CertificateSetup
    {
        public static readonly string certificateCrtEnvVar = "CertificateCrt";
        public static readonly string certificateKeyEnvVar = "CertificateKey";

        public static void EnableTls(this IWebHostBuilder webHostBuilder) 
        {
            if (CanSetup())
            {
                webHostBuilder.ConfigureKestrel((KestrelServerOptions serverOptions) =>
                {
                    serverOptions.ConfigureHttpsDefaults(listenOptions =>
                    {                        
                        listenOptions.ServerCertificate = GetCert2();                        
                    });

                    serverOptions.ListenAnyIP(443, listenOptions =>
                    {
                        listenOptions.Protocols = HttpProtocols.Http2;
                        listenOptions.UseHttps();
                    });
                });
            }
        }

        private static X509Certificate2? GetCert2()
        {
            var certFilePath = Path.GetTempPath() + Guid.NewGuid().ToString() + ".crt";
            var certKeyFilePath = Path.ChangeExtension(certFilePath, "key");

            CreateCrtFile(certFilePath);
            CreateKeyFile(certKeyFilePath);

            var result = X509Certificate2.CreateFromPemFile(certFilePath, certKeyFilePath);

            return result;
        }

        private static bool CanSetup()
        {
            var canSetup = Environment.GetEnvironmentVariable(certificateCrtEnvVar) != null &&
                Environment.GetEnvironmentVariable(certificateKeyEnvVar) != null;

            if (canSetup)
            {
                Console.WriteLine("Found certificate crt and key env. variables.");
            }        
            else 
            {
                Console.Write("Cound not find certificate crt and key env. variables.");
            }

            return canSetup;
        }

        private static void CreateCrtFile(string filePath)
        {
            CreateFileFromEnvVar(certificateCrtEnvVar, filePath);
        }

        private static void CreateKeyFile(string filePath)
        {
            CreateFileFromEnvVar(certificateKeyEnvVar, filePath);
        }

        private static void CreateFileFromEnvVar(string envVar, string filePath)
        {            
            using (StreamWriter sw = File.CreateText(filePath))
            {
                sw.WriteLine(Environment.GetEnvironmentVariable(envVar));
            }
        }
    }
}