﻿using Domain.Models;
using SharedKernel.Interfaces;

namespace UseCases.Cameras.Status
{
    public class CameraStatusCheckUseCase
    {
        private readonly IRepository<Camera> repository;

        public CameraStatusCheckUseCase(IRepository<Camera> repository) 
        {
            this.repository = repository;
        }

        public virtual bool GetByApiKey(string cameraApiKey)
        {
            var camera = repository.FirstOrDefault(x => x.ApiKey == cameraApiKey);

            if (camera == null)
            {
                throw new Exception($"Camera not found. Api key: {cameraApiKey}");
            }

            return camera.MotionDetection;
        }
    }
}
