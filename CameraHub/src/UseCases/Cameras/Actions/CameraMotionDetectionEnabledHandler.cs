﻿using Domain.Models;
using MassTransit;
using SharedKernel.Events;
using SharedKernel.Interfaces;
using UseCases.Cameras.Actions;
using UseCases.Cameras.Actions.Dtos;

namespace UseCases.Cameras.Add
{
    public class CameraMotionDetectionEnabledHandler : IConsumer<CameraMotionDetectionEnabled>
    {        
        private readonly CameraMotionDetectionEvent motionDetectionEvent;
        private readonly IRepository<Camera> repository;

        public CameraMotionDetectionEnabledHandler(
            CameraMotionDetectionEvent motionDetectionEvent,
            IRepository<Camera> repository)
        {
            this.motionDetectionEvent = motionDetectionEvent;
            this.repository = repository;
        }

        public async Task Consume(ConsumeContext<CameraMotionDetectionEnabled> context)
        {
            var camera = repository.Get(context.Message.CameraId);
            camera.EnableMotionDetection();
            repository.Update(camera);
            await repository.SaveChangesAsync();

            var response = new CameraMotionDetectionResponse() 
            {                 
                Status = true,
                ApiKey = camera.ApiKey,
            };

            motionDetectionEvent.Invoke(response);
        }
    }
}
