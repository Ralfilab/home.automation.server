﻿using Domain.Models;
using MassTransit;
using SharedKernel.Events;
using SharedKernel.Interfaces;
using UseCases.Cameras.Actions;
using UseCases.Cameras.Actions.Dtos;

namespace UseCases.Cameras.Add
{
    public class CameraMotionDetectionDisabledHandler : IConsumer<CameraMotionDetectionDisabled>
    {        
        private readonly CameraMotionDetectionEvent motionDetectionEvent;
        private readonly IRepository<Camera> repository;

        public CameraMotionDetectionDisabledHandler(
            CameraMotionDetectionEvent motionDetectionEvent,
            IRepository<Camera> repository)
        {
            this.motionDetectionEvent = motionDetectionEvent;
            this.repository = repository;
        }

        public async Task Consume(ConsumeContext<CameraMotionDetectionDisabled> context)
        {
            var camera = repository.Get(context.Message.CameraId);
            camera.DisableMotionDetection();
            repository.Update(camera);
            await repository.SaveChangesAsync();

            var response = new CameraMotionDetectionResponse() 
            {                 
                Status = false,
                ApiKey = camera.ApiKey,
            };

            motionDetectionEvent.Invoke(response);            
        }
    }
}
