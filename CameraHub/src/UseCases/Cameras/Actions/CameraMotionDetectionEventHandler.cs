﻿using UseCases.Cameras.Actions.Dtos;

namespace UseCases.Cameras.Actions
{
    public class CameraMotionDetectionEvent
    {
        public event EventHandler<CameraMotionDetectionResponse>? Updated;

        public void Invoke(CameraMotionDetectionResponse response)
        {
            Updated?.Invoke(this, response);
        }
    }
}
