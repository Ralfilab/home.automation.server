﻿using MediatR;

namespace UseCases.Cameras.Actions.Dtos
{
    public class CameraMotionDetectionResponse
    {
        public bool Status { get; set; }

        public string ApiKey { get; set; }
    }
}
