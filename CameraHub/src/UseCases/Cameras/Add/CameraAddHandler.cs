﻿using Domain.Models;
using MassTransit;
using SharedKernel.Events;
using SharedKernel.Interfaces;

namespace UseCases.Cameras.Add
{
    public class CameraAddHandler : IConsumer<CameraAdded>
    {
        private readonly IRepository<Camera> repository;

        public CameraAddHandler(IRepository<Camera> repository)
        {
            this.repository = repository;
        }

        public async Task Consume(ConsumeContext<CameraAdded> context)
        {
            var camera = new Camera(context.Message.CameraId, context.Message.ApiKey);

            repository.Add(camera);
            await repository.SaveChangesAsync();
        }
    }
}
