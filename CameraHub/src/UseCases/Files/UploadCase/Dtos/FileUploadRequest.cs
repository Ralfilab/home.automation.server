﻿namespace UseCases.Files.UploadCase.Dtos
{
    public class FileUploadRequest
    {
        public string CameraApiKey { get; set; }

        public string FileExtension { get; set; }

        public byte[] FileBytes { get; set; }        
    }
}
