﻿using UseCases.Interfaces;
using UseCases.Interfaces.Storage.Dtos;
using UseCases.Files.UploadCase.Dtos;
using MassTransit;
using SharedKernel.Events.Cameras;

namespace UseCases.Files.UploadCase
{    
    public class FileUploadForCamera
    {        
        private readonly IStorage storage;
        private readonly IBus bus;

        public FileUploadForCamera(            
            IStorage storage,
            IBus bus)
        {
            this.storage = storage;
            this.bus = bus;
        }

        public virtual async Task Add(FileUploadRequest request)
        {            
            var storageDto = new StorageAddRequest() { 
                FileExtension = request.FileExtension,
                FileBytes = request.FileBytes
            };

            var storageId = await storage.AddAsync(storageDto);            

            var message = new CameraImageUploaded() 
            { 
                StorageId = storageId,
                ApiKey = request.CameraApiKey
            };

            await bus.Publish(message);
        }
    }
}
