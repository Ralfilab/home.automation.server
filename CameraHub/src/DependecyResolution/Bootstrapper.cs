﻿using Infrastructure.DatabaseContexts;
using Infrastructure.Repositories;
using JasperFx.CodeGeneration.Frames;
using Lamar.Microsoft.DependencyInjection;
using MassTransit;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SharedKernel.Interfaces;
using StorageAdapter;
using System.Reflection;
using UseCases.Cameras.Actions;
using UseCases.Cameras.Add;
using UseCases.Interfaces;

namespace DependecyResolution
{
    public class Bootstrapper
    {
        public static void Configure(
            IHostBuilder hostBuilder, 
            IServiceCollection serviceCollection)
        {            
            serviceCollection.AddDbContext<CameraDatabaseContext>(options =>
                options.UseSqlServer(Environment.GetEnvironmentVariable("CameraHubConnectionString"),
                opt => opt.EnableRetryOnFailure(
                    maxRetryCount: 20,
                    maxRetryDelay: TimeSpan.FromSeconds(30),
                    errorNumbersToAdd: null
                )));                        

            hostBuilder.UseLamar((context, registry) =>
            {                
                registry.For(typeof(IRepository<>)).Use(typeof(Repository<>));
                registry.AddSingleton<CameraMotionDetectionEvent>();
                registry.For<IStorage>().Use<AzureBlobStorageAdapter>();
            });                      
        }
    }
}
