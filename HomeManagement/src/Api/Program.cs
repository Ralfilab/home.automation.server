using Api.Hubs;
using Api.Hubs.Handlers;
using DependecyResolution;
using MassTransit;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Mvc;
using System.Reflection;
using System.Text.Json;
using UseCases.Cameras.Add;
using Microsoft.Identity.Web;

namespace Api
{
    public class Program
    {
        public static void Main(string[] args)
        {            
            var builder = WebApplication.CreateBuilder(args);            

            builder.Services.AddControllers().ConfigureApiBehaviorOptions(opt=>
            {
                opt.InvalidModelStateResponseFactory =
                    (context => {
                        return new BadRequestObjectResult(context.ModelState);
                    });                
            })
            .AddJsonOptions(opts => opts.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase)
            .AddJsonOptions(opts => opts.JsonSerializerOptions.DictionaryKeyPolicy = JsonNamingPolicy.CamelCase);
            
            builder.Services.AddAuthorization();

            Bootstrapper.Configure(builder.Host, builder.Services, builder.Configuration);

            builder.Services.AddMassTransit(x =>
            {                
                x.SetKebabCaseEndpointNameFormatter();

                var useCaseAssembly = Assembly.GetAssembly(typeof(CameraAddUseCase));
                var apiAssembly = Assembly.GetAssembly(typeof(CameraImageAddedHandler));

                x.AddConsumers(useCaseAssembly);
                x.AddConsumers(apiAssembly);

                if (builder.Environment.IsDevelopment())
                {
                    x.UsingRabbitMq((context, cfg) =>
                    {
                        cfg.Host("localhost", "/", h =>
                        {
                            h.Username("guest");
                            h.Password("guest");
                        });
                        cfg.ConfigureEndpoints(context);
                    });
                }
                else
                {
                    x.UsingAzureServiceBus((context, cfg) =>
                    {
                        cfg.Host(Environment.GetEnvironmentVariable("AzureBusConnectionString"));
                        cfg.ConfigureEndpoints(context);
                    });
                }                                
            });            

            // https://stackoverflow.com/questions/75365116/azuread-authentication-audience-validation-failed-audiences-did-not-match
            builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddMicrosoftIdentityWebApi(builder.Configuration.GetSection("AzureAd"));

            var corsPolicyName = "ApiCorsPolicy";

            if (builder.Environment.IsDevelopment())
            {                
                builder.Services.AddCors(options =>
                {
                    options.AddPolicy(corsPolicyName,
                    policy =>
                        {
                            policy
                            .WithOrigins(builder.Configuration["Cors"])
                            .AllowCredentials()
                            .AllowAnyHeader()
                            .AllowAnyMethod();
                        });
                    });
            }

            builder.Services.AddSignalR();

            var app = builder.Build();
            
            if (builder.Environment.IsDevelopment())
            {
                app.UseCors(corsPolicyName);
            }

            app.UseAuthentication();
            app.UseAuthorization();

            app.MapControllers();

            app.MapHub<HomeManagementHub>("/hubs/homeManagement");

            app.Run();
        }
    }
}