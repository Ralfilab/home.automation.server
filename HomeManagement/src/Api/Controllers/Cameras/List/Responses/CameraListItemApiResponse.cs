﻿namespace Api.Controllers.Cameras.List
{
    public class CameraListItemApiResponse
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public bool EnableMotionDetection { get; set; }
    }
}
