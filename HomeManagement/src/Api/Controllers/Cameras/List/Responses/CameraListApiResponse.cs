﻿namespace Api.Controllers.Cameras.List
{
    public class CameraListApiResponse
    {
        public IEnumerable<CameraListItemApiResponse> Items { get; set; }
    }
}
