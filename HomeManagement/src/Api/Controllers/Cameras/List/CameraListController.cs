﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using UseCases.Cameras.List;
using UseCases.Utils;

namespace Api.Controllers.Cameras.List
{
    [ApiController]
    [Route("api/camera")]
    public class CameraListController : Controller
    {
        private readonly ListCamerasUseCase useCase;

        public CameraListController(ListCamerasUseCase useCase)
        {
            this.useCase = useCase;
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Get([FromQuery] CameraListApiRequest request)
        {
            IActionResult result;

            if (ModelState.IsValid)
            {
                var useCaseRequest = new ListCameraUseCaseRequest();
                useCaseRequest.MapPaging(request);

                var useCaseResponse = await useCase.Get(useCaseRequest);

                var response = new CameraListApiResponse()
                {
                    Items = useCaseResponse.Items.Select(x => new CameraListItemApiResponse()
                    {
                        Id = x.Id,
                        Name = x.Name,
                        EnableMotionDetection = x.EnableMotionDetection
                    })
                };

                result = Ok(response);
            }
            else
            {
                result = BadRequest();
            }

            return result;
        }
    }
}
