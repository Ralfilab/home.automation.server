﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using UseCases.Cameras.Add;
using UseCases.Cameras.Add.Dtos;

namespace Api.Controllers.Cameras.Add
{
    [ApiController]
    [Route("api/camera")]
    public class CameraAddController : Controller
    {
        private readonly CameraAddUseCase addCameraUseCase;

        public CameraAddController(
            CameraAddUseCase addCameraUseCase
            )
        {
            this.addCameraUseCase = addCameraUseCase;
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Post(CameraAddApiRequest request)
        {
            IActionResult result;

            if (ModelState.IsValid)
            {
                var useCaseDto = new CameraAddRequest()
                {
                    Name = request.Name
                };

                var response = await addCameraUseCase.Add(useCaseDto);

                result = Ok(response);
            }
            else
            {
                result = BadRequest();
            }

            return result;
        }
    }
}