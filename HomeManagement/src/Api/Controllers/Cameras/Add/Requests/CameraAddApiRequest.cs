﻿using System.ComponentModel.DataAnnotations;

namespace Api.Controllers.Cameras.Add
{
    public class CameraAddApiRequest
    {
        [Required]
        public string Name { get; set; }
    }
}
