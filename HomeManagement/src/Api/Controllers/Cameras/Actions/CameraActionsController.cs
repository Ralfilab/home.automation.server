﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using UseCases.Cameras.MotionDetection;

namespace Api.Controllers.Cameras.Actions
{
    [ApiController]
    [Route("api/camera")]
    public class CameraActionsController : Controller
    {        
        private readonly CameraMotionDetectionUseCase motionDetectionUseCase;
        private readonly CameraStandByUseCase standByUseCase;

        public CameraActionsController(            
            CameraMotionDetectionUseCase motionDetectionUseCase,
            CameraStandByUseCase standByUseCase
            )
        {            
            this.motionDetectionUseCase = motionDetectionUseCase;
            this.standByUseCase = standByUseCase;
        }

        [HttpPut("{id}/motiondetection")]
        [Authorize]
        public async Task<IActionResult> MotionDetection(int id)
        {
            await motionDetectionUseCase.Set(id);

            return Ok();
        }

        [HttpPut("{id}/standby")]
        [Authorize]
        public async Task<IActionResult> StandBy(int id)
        {
            await standByUseCase.Set(id);

            return Ok();
        }
    }
}