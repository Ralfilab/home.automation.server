﻿namespace Api.Controllers.CamerasImages
{
    public class CameraImagesListItemApiResponse
    {
        public long Id { get; set; }
        public string Thumbnail { get; set; }
        public string CreatedOn { get; set; }
    }
}
