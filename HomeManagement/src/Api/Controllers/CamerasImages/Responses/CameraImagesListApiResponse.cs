﻿namespace Api.Controllers.CamerasImages
{
    public class CameraImagesListApiResponse
    {
        public IEnumerable<CameraImagesListItemApiResponse> Items { get; set; }
    }
}
