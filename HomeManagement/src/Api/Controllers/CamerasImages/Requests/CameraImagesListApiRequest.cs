﻿using SharedKernel.Interfaces;

namespace Api.Controllers.CamerasImages
{
    public class CameraImagesListApiRequest : IPaging
    {
        public CameraImagesListApiRequest()
        {
            PageNumber = 1;
            PageSize = 10;
        }

        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
}
