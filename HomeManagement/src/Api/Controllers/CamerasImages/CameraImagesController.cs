﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using UseCases.CameraImages;
using UseCases.CameraImages.List;
using UseCases.Utils;

namespace Api.Controllers.CamerasImages
{
    [ApiController]
    [Route("api/[controller]")]
    public class CameraImageController : Controller
    {
        private readonly CameraImagesListUseCase useCase;

        public CameraImageController(CameraImagesListUseCase useCase)
        {
            this.useCase = useCase;
        }

        [HttpGet]
        [Authorize]
        public IActionResult Get([FromQuery] CameraImagesListApiRequest request)
        {
            IActionResult result;

            if (ModelState.IsValid)
            {
                var useCaseRequest = new ListCameraImagesUseCaseRequest();
                useCaseRequest.MapPaging(request);

                var useCaseResponse = useCase.Get(useCaseRequest);

                var response = new CameraImagesListApiResponse()
                {
                    Items = useCaseResponse.Items.Select(x => new CameraImagesListItemApiResponse()
                    {
                        Id = x.Id,
                        Thumbnail = x.Thumbnail.AbsoluteUri,
                        CreatedOn = $"{x.CreatedOn.ToShortDateString()} {x.CreatedOn.ToShortTimeString()}"
                    })
                };

                result = Ok(response);
            }
            else
            {
                result = BadRequest();
            }

            return result;
        }

    }
}
