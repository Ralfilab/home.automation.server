﻿using UseCases.CameraImages;

namespace Api.Hubs
{
    public interface IHomeManagementHub
    {
        Task CameraImageAdded(CameraImagesListItemResponse response);
    }
}