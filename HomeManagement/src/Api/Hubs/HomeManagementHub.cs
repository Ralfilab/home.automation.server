﻿using Microsoft.AspNetCore.SignalR;
using UseCases.CameraImages;

namespace Api.Hubs
{
    public class HomeManagementHub : Hub<IHomeManagementHub>
    {
        public async Task CameraImageAdded(CameraImagesListItemResponse response)
        {
            await Clients.All.CameraImageAdded(response);
        }
    }
}
