﻿using MassTransit;
using Microsoft.AspNetCore.SignalR;
using SharedKernel.Events.Cameras;
using UseCases.CameraImages.List;

namespace Api.Hubs.Handlers
{
    public class CameraImageAddedHandler : IConsumer<CameraImageAdded>
    {
        private readonly IHubContext<HomeManagementHub, IHomeManagementHub> hubContext;
        private readonly CameraImagesListUseCase useCase;

        public CameraImageAddedHandler(IHubContext<HomeManagementHub, IHomeManagementHub> hubContext, CameraImagesListUseCase useCase)
        {
            this.hubContext = hubContext;
            this.useCase = useCase;
        }

        public async Task Consume(ConsumeContext<CameraImageAdded> context)
        {
            var response = useCase.Get(context.Message.CameraImageId);

            await hubContext.Clients.All.CameraImageAdded(response);
        }
    }
}
