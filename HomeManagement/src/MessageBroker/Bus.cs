﻿using System.Collections.Concurrent;
using System.Text.Json;
using Azure.Messaging.ServiceBus;
using Microsoft.Extensions.Configuration;
using SharedKernel.Interfaces;

namespace MessageBroker
{
    public class Bus : IBus
    {
        private ServiceBusClient client;
        private ConcurrentDictionary<string, ServiceBusSender> senders;        

        public Bus(IConfiguration configuration)
        {
            client = new ServiceBusClient(configuration.GetConnectionString("BusConnection"));
            senders = new ConcurrentDictionary<string, ServiceBusSender>();
        }

        public async Task Send<T>(T message, string queueName)
        {
            // https://github.com/Azure/azure-sdk-for-net/blob/main/sdk/servicebus/Azure.Messaging.ServiceBus/README.md
            // https://damienbod.com/2019/04/23/using-azure-service-bus-queues-with-asp-net-core-services/
            // https://www.kambu.pl/blog/microservices-communication-using-azure-service-bus/                    

            var sender = senders.GetOrAdd(queueName, (queueName) => {
                return client.CreateSender(queueName);
            });
                
            var serializedClass = JsonSerializer.Serialize(message);
            ServiceBusMessage azureMessage = new ServiceBusMessage(serializedClass);
            await sender.SendMessageAsync(azureMessage);

            //// The receiver is responsible for reading messages from the queue.
            //ServiceBusReceiver receiver = client.CreateReceiver(queueName);
            //ServiceBusReceivedMessage receivedMessage = await receiver.ReceiveMessageAsync();

            //string body = receivedMessage.Body.ToString();            
        }
    }
}