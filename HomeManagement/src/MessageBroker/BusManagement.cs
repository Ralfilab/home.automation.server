﻿using Azure.Messaging.ServiceBus.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MessageBroker
{
    public class BusManagement
    {
        public async Task CreateQueue(string queueName) 
        {
            string connectionString = "$%^&*(";            
            
            var adminClient = new ServiceBusAdministrationClient(connectionString);
            if (!await adminClient.QueueExistsAsync(queueName)) 
            {
                await adminClient.CreateQueueAsync(queueName);
            }                            
        }    
    }
}
