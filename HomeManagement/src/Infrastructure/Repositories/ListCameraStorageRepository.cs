﻿using Infrastructure.DatabaseContexts;
using Microsoft.EntityFrameworkCore;
using Domain.Models.HomeAutomation;
using UseCases.CameraImages.Repository.ListCameraImages.Dtos.Requests;
using UseCases.CameraImages.List.Repository.ListCameraImages;
using UseCases.CameraImages.List.Repository.ListCameraImages.Dtos.Responses;

namespace Infrastructure.Repositories
{
    public class ListCameraStorageRepository : Repository<CameraStorage>, IListCameraStorageRepository
    { 
        public ListCameraStorageRepository(
            IDbContextProvider contextProvider) : base(contextProvider)
        { }

        public IEnumerable<ListCameraImagesItemRepositoryResponse> List(ListCameraImagesRepositoryRequest request)
        {                        
            var skip = (request.PageNumber - 1) * request.PageSize;

            var result = entities.Select(
                x => new ListCameraImagesItemRepositoryResponse()
                { 
                    Id = x.Id,
                    StorageId = x.StorageId,
                    CreatedOn = x.CreatedOn
                })
            .Skip(skip)
            .Take(request.PageSize)
            .AsNoTrackingWithIdentityResolution();

            return result;
        }        
    }
}
