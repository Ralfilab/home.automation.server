﻿using Infrastructure.DatabaseContexts;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;
using SharedKernel;
using SharedKernel.Interfaces;
using MediatR;

namespace Infrastructure.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity>
        where TEntity : Entity
    {
        protected readonly DbContext dbContext;
        protected readonly DbSet<TEntity> entities;
        private readonly IMediator mediator;

        public Repository(IDbContextProvider contextProvider)
        {
            this.dbContext = contextProvider.GetDbContext();

            this.entities = this.dbContext.Set<TEntity>();            
        }

        public Repository(IDbContextProvider contextProvider, IMediator mediator) 
            : this(contextProvider)
        {            
            this.mediator = mediator;
        }

        public TEntity Get(long id)
        {
            return entities.Find(id);
        }

        public TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate)
        {            
            return entities.FirstOrDefault(predicate);
        }

        public async Task<IEnumerable<TEntity>> ListReadOnlyAsync(IPaging paging)
        {
            var skip = (paging.PageNumber - 1) * paging.PageSize;

            var result = await entities
                .Skip(skip)
                .Take(paging.PageSize)
                .AsNoTrackingWithIdentityResolution()
                .ToListAsync();

            return result;
        }

        public void Update(TEntity entity)
        {
            AttachIfNot(entity);

            dbContext.Entry(entity).State = EntityState.Modified;            
        }

        public TEntity Add(TEntity entity)
        {
            AttachIfNot(entity);

            dbContext.Entry(entity).State = EntityState.Added;

            return entities.Add(entity).Entity;
        }

        public async Task<TEntity> AddAsync(TEntity entity)
        {
            AttachIfNot(entity);

            dbContext.Entry(entity).State = EntityState.Added;

            var result = await entities.AddAsync(entity);

            return result.Entity;
        }

        public async Task<int> CountAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await entities.Where(predicate).CountAsync();
        }

        public async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            var result = await dbContext.SaveChangesAsync(cancellationToken).ConfigureAwait(false);

            if (mediator != null)
            {
                var entitiesWithEvents = dbContext.ChangeTracker
                  .Entries()
                  .Select(e => e.Entity as AggregateRoot)
                  .Where(e => e?.DomainEvents != null && e.DomainEvents.Any())
                  .ToArray();

                    foreach (var entity in entitiesWithEvents)
                    {
                        var events = entity.DomainEvents.ToArray();
                        entity.ClearEvents();

                        foreach (var @event in events)
                        {
                            await mediator.Publish(@event, cancellationToken);
                        }
                    }
            }            

            return result;
        }

        protected virtual void AttachIfNot(TEntity entity)
        {
            if (entities.Local.Contains(entity) == false)
            {
                entities.Attach(entity);
            }
        }
    }
}
