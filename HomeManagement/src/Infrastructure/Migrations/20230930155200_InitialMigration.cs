﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class InitialMigration : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "home_management");

            migrationBuilder.CreateSequence(
                name: "seq_api_key",
                schema: "home_management",
                incrementBy: 10);

            migrationBuilder.CreateSequence(
                name: "seq_camera",
                schema: "home_management",
                incrementBy: 10);

            migrationBuilder.CreateSequence(
                name: "seq_camera_storage",
                schema: "home_management",
                incrementBy: 10);

            migrationBuilder.CreateTable(
                name: "api_key",
                schema: "home_management",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false),
                    Key = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_api_key", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "camera",
                schema: "home_management",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    MotionDetection = table.Column<bool>(type: "bit", nullable: false),
                    ApiKeyId = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_camera", x => x.Id);
                    table.ForeignKey(
                        name: "FK_camera_api_key_ApiKeyId",
                        column: x => x.ApiKeyId,
                        principalSchema: "home_management",
                        principalTable: "api_key",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "camera_storage",
                schema: "home_management",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false),
                    CameraId = table.Column<long>(type: "bigint", nullable: false),
                    StorageId = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    CreatedOn = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_camera_storage", x => x.Id);
                    table.ForeignKey(
                        name: "FK_camera_storage_camera_CameraId",
                        column: x => x.CameraId,
                        principalSchema: "home_management",
                        principalTable: "camera",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_camera_ApiKeyId",
                schema: "home_management",
                table: "camera",
                column: "ApiKeyId");

            migrationBuilder.CreateIndex(
                name: "IX_camera_storage_CameraId",
                schema: "home_management",
                table: "camera_storage",
                column: "CameraId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "camera_storage",
                schema: "home_management");

            migrationBuilder.DropTable(
                name: "camera",
                schema: "home_management");

            migrationBuilder.DropTable(
                name: "api_key",
                schema: "home_management");

            migrationBuilder.DropSequence(
                name: "seq_api_key",
                schema: "home_management");

            migrationBuilder.DropSequence(
                name: "seq_camera",
                schema: "home_management");

            migrationBuilder.DropSequence(
                name: "seq_camera_storage",
                schema: "home_management");
        }
    }
}
