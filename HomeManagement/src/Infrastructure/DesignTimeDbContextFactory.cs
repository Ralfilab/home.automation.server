﻿using Infrastructure.DatabaseContexts;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Infrastructure.Migrations
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<CameraDatabaseContext>
    {
        public CameraDatabaseContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<CameraDatabaseContext>();
            optionsBuilder.UseSqlServer(
                "Server=OFFICEDESKTOP\\SQLEXPRESS;Database=HomeAutomation;" +
                "trusted_connection=true;Encrypt=False;",
                b => b.MigrationsAssembly("Infrastructure"));

            return new CameraDatabaseContext(optionsBuilder.Options);
        }
    }
}
