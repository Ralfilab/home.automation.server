﻿using Infrastructure.DatabaseContexts.Configuration;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.DatabaseContexts
{
    public class CameraDatabaseContext : DbContext, IDbContextProvider
    {
        public CameraDatabaseContext(DbContextOptions options)
            : base(options)
        {
        }

        public DbContext GetDbContext()
        {
            return this;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("home_management");

            modelBuilder.ApplyConfiguration(new CameraStorageEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CameraEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ApiKeyEntityTypeConfiguration());
        }
    }
}
