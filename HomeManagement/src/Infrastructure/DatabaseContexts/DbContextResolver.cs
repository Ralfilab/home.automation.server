﻿using Microsoft.EntityFrameworkCore;

namespace Infrastructure.DatabaseContexts
{
    public class DbContextResolver : IDbContextProvider
    {
        private readonly DbContext dbContext;

        public DbContextResolver(CameraDatabaseContext databaseContext)
        {
            this.dbContext = databaseContext;
        }

        public DbContext GetDbContext()
        {
            return dbContext; 
        }
    }
}
