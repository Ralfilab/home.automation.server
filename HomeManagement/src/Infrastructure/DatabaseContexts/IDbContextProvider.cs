﻿using Microsoft.EntityFrameworkCore;

namespace Infrastructure.DatabaseContexts
{
    public interface IDbContextProvider
    {
         DbContext GetDbContext();
    }
}
