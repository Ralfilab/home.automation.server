﻿using Domain.Models.HomeAutomation;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.DatabaseContexts
{
    internal class CameraStorageEntityTypeConfiguration : IEntityTypeConfiguration<CameraStorage>
    {
        public void Configure(EntityTypeBuilder<CameraStorage> builder)
        {
            builder.ToTable("camera_storage");

            builder.HasOne(x => x.Camera);

            builder.HasKey(o => o.Id);

            builder.Property(o => o.Id)
                .UseHiLo("seq_camera_storage");
        }
    }
}
