﻿using Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.DatabaseContexts.Configuration
{
    internal class ApiKeyEntityTypeConfiguration : IEntityTypeConfiguration<ApiKey>
    {
        public void Configure(EntityTypeBuilder<ApiKey> builder)
        {
            builder.ToTable("api_key");

            builder.HasKey(o => o.Id);

            builder.Property(o => o.Id)
                .UseHiLo("seq_api_key");
        }
    }
}
