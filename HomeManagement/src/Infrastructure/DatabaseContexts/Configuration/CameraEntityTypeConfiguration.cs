﻿using Domain.Models;
using Domain.Models.HomeAutomation;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.DatabaseContexts.Configuration
{
    internal class CameraEntityTypeConfiguration : IEntityTypeConfiguration<Camera>
    {
        public void Configure(EntityTypeBuilder<Camera> builder)
        {
            builder.ToTable("camera");

            builder.HasKey(o => o.Id);

            builder.Property(o => o.Id)
                .UseHiLo("seq_camera");

            builder.Ignore(x => x.DomainEvents);

            builder.HasOne(x => x.ApiKey);
        }
    }
}
