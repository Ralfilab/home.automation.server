﻿using SharedKernel;

namespace Domain.Models
{
    public class ApiKey : Entity
    {
        public string Key { get; set; }

        // TODO: Add max number of retries to block hacking attempts

        protected ApiKey()
        { }

        public ApiKey(string key)
        {            
            Key = key;            
        }
    }
}