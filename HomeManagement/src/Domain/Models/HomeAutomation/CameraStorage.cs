﻿using SharedKernel;

namespace Domain.Models.HomeAutomation
{
    public class CameraStorage : Entity
    {
        protected CameraStorage()
        { 
            // only for mapping
        }

        public CameraStorage(Camera camera, string storageId)
        {
            CreatedOn = DateTime.UtcNow;
            Camera = camera;
            StorageId = storageId;
        }       

        public Camera Camera { get; private set; }

        public string StorageId { get; private set; }

        public DateTime CreatedOn { get; private set; }        
    }
}
