﻿using SharedKernel.Interfaces;

namespace Domain.Models.HomeAutomation.Events
{
    public class CameraMotionDetectionEnabled : DomainEvent
    {
        public Camera Camera { get; init; }
    }
}