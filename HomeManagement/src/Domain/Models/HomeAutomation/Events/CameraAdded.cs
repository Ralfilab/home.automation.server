﻿using SharedKernel.Interfaces;

namespace Domain.Models.HomeAutomation.Events
{
    public class CameraAdded : DomainEvent
    {
        public Camera Camera { get; init; }
    }
}