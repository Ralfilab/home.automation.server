﻿using Domain.Models.HomeAutomation.Events;
using SharedKernel;

namespace Domain.Models.HomeAutomation
{
    public class Camera : AggregateRoot
    {        
        public string Name { get; set; }

        public bool MotionDetection { get; private set; }

        public ApiKey ApiKey { get; protected set; }

        private Camera() { }

        public Camera(string name)
        { 
            Name = name;
            ApiKey = new ApiKey(Guid.NewGuid().ToString());
            AddDomainEvent(
                new CameraAdded() {
                    Camera = this
                });
        }

        public void EnableMotionDetection() 
        {
            MotionDetection = true;
            AddDomainEvent(
                new CameraMotionDetectionEnabled() { 
                    Camera = this 
                });
        }

        public void DisableMotionDetection()
        {
            MotionDetection = false;
            AddDomainEvent(
                new CameraMotionDetectionDisabled()
                {
                    Camera = this
                });
        }
    }
}