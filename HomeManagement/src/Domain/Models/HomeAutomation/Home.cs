﻿using SharedKernel;

namespace Domain.Models.HomeAutomation
{
    public class Home : Entity
    {
        public string Name { get; set; }        

        public Home(string name)
        {
            Name = name;
        }
    }
}