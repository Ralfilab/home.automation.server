﻿using Domain.Models.HomeAutomation;
using SharedKernel.Interfaces;

namespace UseCases.Cameras.List
{
    public class ListCamerasUseCase
    {
        private IRepository<Camera> repository;

        public ListCamerasUseCase(IRepository<Camera> repository)
        {
            this.repository = repository;
        }

        public async Task<CameraListResponse> Get(ListCameraUseCaseRequest useCaseRequest)
        {
            var cameras = await repository.ListReadOnlyAsync(useCaseRequest);

            var response = new CameraListResponse()
            {
                Items = cameras.Select((x, index) => new CamerasListItemResponse()
                {
                    Id = x.Id,
                    Name = x.Name,
                    EnableMotionDetection = x.MotionDetection
                })
            };

            return response;
        }
    }
}
