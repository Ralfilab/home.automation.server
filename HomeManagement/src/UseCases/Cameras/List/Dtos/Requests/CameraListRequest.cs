﻿using SharedKernel.Interfaces;

namespace UseCases.Cameras.List
{
    public record ListCameraUseCaseRequest : IPaging
    {
        public ListCameraUseCaseRequest()
        {
            PageNumber = 1;
            PageSize = 20;
        }

        public int PageNumber { get; set; }
        public int PageSize { get; set; }       
    }
}
