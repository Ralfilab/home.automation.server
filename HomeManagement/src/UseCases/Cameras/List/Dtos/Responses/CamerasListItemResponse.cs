﻿namespace UseCases.Cameras.List
{
    public record CamerasListItemResponse
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public bool EnableMotionDetection { get; set; }
    }
}
