﻿namespace UseCases.Cameras.List
{
    public record CameraListResponse
    {        
        public CameraListResponse() 
        {
            Items = Enumerable.Empty<CamerasListItemResponse>();
        }

        public IEnumerable<CamerasListItemResponse> Items { get; set; }
    }
}