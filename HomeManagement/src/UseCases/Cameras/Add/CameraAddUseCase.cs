﻿using UseCases.Cameras.Add.Dtos;
using Domain.Models.HomeAutomation;
using UseCases.CameraImages.Repository;
using SharedKernel.Interfaces;

namespace UseCases.Cameras.Add
{
    public class CameraAddUseCase
    {
        private readonly IRepository<Camera> repository;

        public CameraAddUseCase(IRepository<Camera> repository)
        {
            this.repository = repository;
        }

        public async Task<CameraAddResponse> Add(CameraAddRequest dto)
        {
            var camera = new Camera(dto.Name);

            camera = await repository.AddAsync(camera);

            await repository.SaveChangesAsync();

            var result = new CameraAddResponse() 
            { 
                Id = camera.Id,
                Name = camera.Name,
                ApiKey = camera.ApiKey.Key
            };

            return result;
        }
    }
}
