﻿namespace UseCases.Cameras.Add.Dtos
{
    public class CameraAddResponse
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string ApiKey { get; set; }
    }
}
