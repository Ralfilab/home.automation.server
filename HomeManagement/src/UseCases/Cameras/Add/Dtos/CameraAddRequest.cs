﻿namespace UseCases.Cameras.Add.Dtos
{
    public class CameraAddRequest
    {
        public string Name { get; set; }
    }
}
