﻿using Domain.Models.HomeAutomation.Events;
using MassTransit;
using MediatR;

namespace UseCases.Cameras.Add.Handlers
{
    public class CameraAddedHandler : INotificationHandler<CameraAdded>
    {
        private readonly IBus bus;

        public CameraAddedHandler(IBus bus)
        {
            this.bus = bus;
        }

        async Task INotificationHandler<CameraAdded>.Handle(CameraAdded notification, CancellationToken cancellationToken)
        {
            var @event = new SharedKernel.Events.CameraAdded()
            {
                CameraId = notification.Camera.Id,
                ApiKey = notification.Camera.ApiKey.Key
            };

            await bus.Publish(@event, cancellationToken);
        }
    }
}
