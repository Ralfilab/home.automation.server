﻿using Domain.Models.HomeAutomation;
using SharedKernel.Interfaces;

namespace UseCases.Cameras.MotionDetection
{
    public class CameraStandByUseCase
    {        
        private IRepository<Camera> repository;

        public CameraStandByUseCase(IRepository<Camera> repository) 
        {             
            this.repository = repository;
        }

        public async Task Set(long id)
        {
            var camera = repository.Get(id);

            ArgumentNullException.ThrowIfNull(camera, nameof(camera));

            camera.DisableMotionDetection();

            repository.Update(camera);

            await repository.SaveChangesAsync();
        }        
    }
}
