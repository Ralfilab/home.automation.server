﻿using Domain.Models.HomeAutomation;
using SharedKernel.Interfaces;

namespace UseCases.Cameras.MotionDetection
{
    public class CameraMotionDetectionUseCase
    {        
        private IRepository<Camera> repository;

        public CameraMotionDetectionUseCase(IRepository<Camera> repository) 
        {             
            this.repository = repository;
        }

        public async Task Set(long id)
        {
            var camera = repository.Get(id);

            ArgumentNullException.ThrowIfNull(camera, nameof(camera));

            camera.EnableMotionDetection();

            repository.Update(camera);

            await repository.SaveChangesAsync();
        }        
    }
}
