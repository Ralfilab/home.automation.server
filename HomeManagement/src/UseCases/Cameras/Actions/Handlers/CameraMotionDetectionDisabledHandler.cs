﻿using Domain.Models.HomeAutomation.Events;
using MassTransit;
using MediatR;

namespace UseCases.Cameras.Actions.Handlers
{
    public class CameraMotionDetectionDisabledHandler : INotificationHandler<CameraMotionDetectionDisabled>
    {
        private readonly IBus bus;

        public CameraMotionDetectionDisabledHandler(IBus bus) 
        {
            this.bus = bus;
        }

        public async Task Handle(CameraMotionDetectionDisabled notification, CancellationToken cancellationToken)
        {
            var @event = new SharedKernel.Events.CameraMotionDetectionDisabled()
            { 
                CameraId = notification.Camera.Id 
            };

            await bus.Publish(@event, cancellationToken);
        }
    }
}
