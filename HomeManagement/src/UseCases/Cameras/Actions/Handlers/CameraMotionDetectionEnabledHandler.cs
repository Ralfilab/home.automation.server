﻿using Domain.Models.HomeAutomation.Events;
using MassTransit;
using MediatR;

namespace UseCases.Cameras.Actions.Handlers
{
    public class CameraMotionDetectionEnabledHandler : INotificationHandler<CameraMotionDetectionEnabled>
    {
        private readonly IBus bus;

        public CameraMotionDetectionEnabledHandler(IBus bus) 
        {
            this.bus = bus;
        }

        public async Task Handle(CameraMotionDetectionEnabled notification, CancellationToken cancellationToken)
        {
            var @event = new SharedKernel.Events.CameraMotionDetectionEnabled()
            { 
                CameraId = notification.Camera.Id 
            };

            await bus.Publish(@event, cancellationToken);            
        }
    }
}
