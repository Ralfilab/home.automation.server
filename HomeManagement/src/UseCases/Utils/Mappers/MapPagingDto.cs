﻿using SharedKernel.Interfaces;

namespace UseCases.Utils
{
    public static class MapPagingDto
    {
        public static void MapPaging<T>(this T to, IPaging from) where T : IPaging
        {
            to.PageNumber = from.PageNumber;
            to.PageSize = from.PageSize;
        }
    }
}