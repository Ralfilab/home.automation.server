﻿using UseCases.CameraImages.List.Dtos.Responses;
using UseCases.CameraImages.List.Repository.ListCameraImages;
using UseCases.CameraImages.Repository.ListCameraImages.Dtos.Requests;
using UseCases.Interfaces;
using UseCases.Utils;

namespace UseCases.CameraImages.List
{
    public class CameraImagesListUseCase
    {
        private readonly IListCameraStorageRepository repository;
        private readonly IStorage storage;

        public CameraImagesListUseCase(
            IListCameraStorageRepository repository, 
            IStorage storage)
        {
            this.repository = repository;
            this.storage = storage;
        }

        public CameraImagesListResponse Get(ListCameraImagesUseCaseRequest request)
        {
            var repositoryRequest = new ListCameraImagesRepositoryRequest();
            repositoryRequest.MapPaging(request);

            var repositoryResponse = repository.List(repositoryRequest);

            var storageResponse = storage.GetUri(repositoryResponse.Select(x => x.StorageId)).ToArray();

            var response = new CameraImagesListResponse()
            {
                Items = repositoryResponse.Select((x, index) => new CameraImagesListItemResponse()
                {
                    Id = x.Id,
                    CreatedOn = x.CreatedOn,
                    Thumbnail = storageResponse[index]
                })
            };

            return response;
        }

        public CameraImagesListItemResponse Get(long cameraImageId)
        {
            var cameraStorage = repository.Get(cameraImageId);

            var storageResponse = storage.GetUri(new[] { cameraStorage.StorageId }).SingleOrDefault();

            var response = new CameraImagesListItemResponse()
            {
                Id = cameraStorage.Id,
                CreatedOn = cameraStorage.CreatedOn,
                Thumbnail = storageResponse
            };

            return response;
        }
    }
}
