﻿namespace UseCases.CameraImages.List.Dtos.Responses
{
    public class CameraImagesListResponse
    {
        public CameraImagesListResponse()
        {
            Items = Enumerable.Empty<CameraImagesListItemResponse>();
        }

        public IEnumerable<CameraImagesListItemResponse> Items { get; set; }
    }
}