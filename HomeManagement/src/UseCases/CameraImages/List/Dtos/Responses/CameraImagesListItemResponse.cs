﻿namespace UseCases.CameraImages
{
    public class CameraImagesListItemResponse
    {
        public long Id { get; set; }
        public Uri Thumbnail { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
