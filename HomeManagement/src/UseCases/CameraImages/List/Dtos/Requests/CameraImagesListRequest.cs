﻿using SharedKernel.Interfaces;

namespace UseCases.CameraImages
{
    public class ListCameraImagesUseCaseRequest : IPaging
    {
        public ListCameraImagesUseCaseRequest()
        {
            PageNumber = 1;
            PageSize = 20;
        }

        public int PageNumber { get; set; }
        public int PageSize { get; set; }       
    }
}
