﻿using SharedKernel.Interfaces;

namespace UseCases.CameraImages.Repository.ListCameraImages.Dtos.Requests
{
    public class ListCameraImagesRepositoryRequest : IPaging
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
}