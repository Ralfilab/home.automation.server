﻿namespace UseCases.CameraImages.List.Repository.ListCameraImages.Dtos.Responses
{
    public class ListCameraImagesItemRepositoryResponse
    {
        public long Id { get; set; }

        public string StorageId { get; set; }

        public DateTime CreatedOn { get; set; }
    }
}
