﻿using Domain.Models.HomeAutomation;
using SharedKernel.Interfaces;
using UseCases.CameraImages.List.Repository.ListCameraImages.Dtos.Responses;
using UseCases.CameraImages.Repository.ListCameraImages.Dtos.Requests;

namespace UseCases.CameraImages.List.Repository.ListCameraImages
{
    public interface IListCameraStorageRepository : IRepository<CameraStorage>
    {
        IEnumerable<ListCameraImagesItemRepositoryResponse> List(ListCameraImagesRepositoryRequest listCameraImagesRepositoryRequest);
    }
}