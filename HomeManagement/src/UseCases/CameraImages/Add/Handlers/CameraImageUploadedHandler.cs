﻿using Domain.Models.HomeAutomation;
using MassTransit;
using SharedKernel.Events.Cameras;
using SharedKernel.Interfaces;

namespace UseCases.CameraImages.Add.Handlers
{
    public class CameraImageUploadedHandler : IConsumer<CameraImageUploaded>
    {
        private readonly IRepository<Camera> repository;
        private readonly IRepository<CameraStorage> cameraStorageRepository;

        public CameraImageUploadedHandler(IRepository<Camera> cameraRepository,
            IRepository<CameraStorage> cameraStorageRepository)
        {
            this.repository = cameraRepository;
            this.cameraStorageRepository = cameraStorageRepository;
        }

        public async Task Consume(ConsumeContext<CameraImageUploaded> context)
        {
            var camera = repository.FirstOrDefault(x => x.ApiKey.Key == context.Message.ApiKey);
            
            var cameraStorage = new CameraStorage(camera, context.Message.StorageId);

            cameraStorageRepository.Add(cameraStorage);

            await cameraStorageRepository.SaveChangesAsync();

            await context.Publish(
                new CameraImageAdded() { 
                    CameraImageId = cameraStorage.Id,
                });
        }
    }
}
