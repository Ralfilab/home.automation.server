﻿namespace UseCases.ApiAuthorization.Dtos
{
    public interface IApiKeyAuthorization
    {
        string ApiKey { get; set; }
    }
}
