﻿using Domain.Models;
using SharedKernel.Interfaces;
using UseCases.ApiAuthorization.Dtos;

namespace UseCases.ApiAuthorization
{
    public class EnsureValidApiKey
    {
        private readonly IRepository<ApiKey> repository;

        public EnsureValidApiKey(IRepository<ApiKey> repository)
        {
            this.repository = repository;
        }

        public async virtual Task<bool> IsValid(IApiKeyAuthorization model)
        { 
            return await repository.CountAsync(x => x.Key == model.ApiKey) == 1;
        }
    }
}
