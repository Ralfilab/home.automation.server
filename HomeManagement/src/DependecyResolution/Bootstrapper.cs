﻿using UseCases.Interfaces;
using Infrastructure.DatabaseContexts;
using Infrastructure.Repositories;
using Lamar.Microsoft.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using StorageAdapter;
using SharedKernel.Interfaces;
using UseCases.Cameras.Add.Handlers;
using UseCases.CameraImages.List.Repository.ListCameraImages;

namespace DependecyResolution
{
    public class Bootstrapper
    {
        public static void Configure(
            IHostBuilder hostBuilder, 
            IServiceCollection serviceCollection, 
            IConfiguration configuration)
        {
            serviceCollection.AddDbContext<CameraDatabaseContext>(options =>
                options.UseSqlServer(Environment.GetEnvironmentVariable("HomeManagementConnectionString"),
                opt => opt.EnableRetryOnFailure(
                    maxRetryCount: 20,
                    maxRetryDelay: TimeSpan.FromSeconds(30),
                    errorNumbersToAdd: null
                ))
            );

            serviceCollection.AddMediatR(
                cfg => cfg.RegisterServicesFromAssemblyContaining<CameraAddedHandler>()
            );            

            hostBuilder.UseLamar((context, registry) =>
            {
                registry.For<IDbContextProvider>().Use<DbContextResolver>();
                registry.For(typeof(IRepository<>)).Use(typeof(Repository<>));
                registry.For<IListCameraStorageRepository>().Use<ListCameraStorageRepository>();                
                registry.For<IStorage>().Use<AzureBlobStorageAdapter>();
            });                        
        }
    }
}
