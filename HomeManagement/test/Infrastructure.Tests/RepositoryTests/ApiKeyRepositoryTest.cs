﻿using Domain.Models;
using Infrastructure.DatabaseContexts;
using Infrastructure.Repositories;
using Xunit;
using SharedKernel.Interfaces;

namespace Infrastructure.Tests.RepositoryTests
{
    [Collection("DatabaseTest")]
    public class ApiKeyRepositoryTest : RepositoryTest
    {        
        public ApiKeyRepositoryTest() : base()
        {
        }

        [Fact]
        public void AddTest()
        {
            var expectedKeyId = Guid.NewGuid();
            
            var expectedApiKey = CreateApiKey(expectedKeyId);

            var repository = GetRepository();

            var actualApiKey = repository.FirstOrDefault(x => x.Key == expectedKeyId.ToString());

            Assert.Equal(expectedApiKey.Id, actualApiKey.Id);            
        }

        private ApiKey CreateApiKey(Guid key)
        {
            ApiKey result;

            using (var context = new CameraDatabaseContext(dbContextOptions))
            {
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();

                var dbSet = context.Set<ApiKey>();

                var apiKey = new ApiKey(key.ToString());

                dbSet.Add(apiKey);

                context.SaveChanges();

                result = apiKey;
            }

            return result;
        }

        private IRepository<ApiKey> GetRepository()
        {
            var dbContextResolver = new DbContextResolver(new CameraDatabaseContext(dbContextOptions));

            var repository = new Repository<ApiKey>(dbContextResolver);

            return repository;
        }
    }
}
