﻿using Infrastructure.DatabaseContexts;
using Infrastructure.Repositories;
using Xunit;
using Domain.Models.HomeAutomation;
using UseCases.CameraImages.Repository.ListCameraImages.Dtos.Requests;
using UseCases.CameraImages.List.Repository.ListCameraImages.Dtos.Responses;

namespace Infrastructure.Tests.RepositoryTests
{
    [Collection("DatabaseTest")]
    public class ListCameraStorageRepositoryTest : RepositoryTest
    {
        public ListCameraStorageRepositoryTest() : base()
        {            
        }

        [Fact]
        public void ListTest()
        {
            var expectedCamera = CreateStorageItems();

            var request = new ListCameraImagesRepositoryRequest() { 
                PageNumber = 2,
                PageSize = 5,
            };

            var repository = GetRepository();            

            var givenResult = repository.List(request);

            var expectedIndex = (request.PageNumber - 1) * request.PageSize;

            Assert.Equal(5, givenResult.Count());

            foreach (var givenItem in givenResult)
            {
                AssertItem(expectedCamera[expectedIndex], givenItem);
                expectedIndex++;
            }
        }

        private void AssertItem(CameraStorage expected, ListCameraImagesItemRepositoryResponse given)
        {
            Assert.Equal(expected.Id, given.Id);
            Assert.Equal(expected.CreatedOn, given.CreatedOn);
            Assert.Equal(expected.StorageId, given.StorageId);
        }

        private IList<CameraStorage> CreateStorageItems()
        {
            var result = new List<CameraStorage>();

            using (var context = new CameraDatabaseContext(dbContextOptions))
            {
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();

                var dbSetCamera = context.Set<Camera>();

                var camera = new Camera("Living room");                

                dbSetCamera.Add(camera);

                var dbSetStorageItem = context.Set<CameraStorage>();

                for (var i = 0; i < 50; i++)
                {                                        
                    var storageItem = new CameraStorage(camera, $"storage name {i}");
                    dbSetStorageItem.Add(storageItem);

                    result.Add(storageItem);
                }

                context.SaveChanges();
            }    
            
            return result;
        }

        private ListCameraStorageRepository GetRepository()
        {
            var dbContextResolver = new DbContextResolver(new CameraDatabaseContext(dbContextOptions));

            var repository = new ListCameraStorageRepository(dbContextResolver);

            return repository;
        }
    }
}
