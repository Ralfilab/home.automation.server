﻿using Infrastructure.DatabaseContexts;
using Infrastructure.Repositories;
using Xunit;
using Domain.Models.HomeAutomation;
using SharedKernel.Interfaces;

namespace Infrastructure.Tests.RepositoryTests
{
    [Collection("DatabaseTest")]
    public class CameraStorageRepositoryTest : RepositoryTest
    {
        public CameraStorageRepositoryTest() : base()
        {            
        }

        [Fact]
        public void AddTest()
        {
            var expectedCamera = CreateStorageItem();

            var cameraRepository = GetRepository();

            var actualCamera = cameraRepository.Get(expectedCamera.Id);

            Assert.Equal(expectedCamera.Id, actualCamera.Id);            
            Assert.Equal(expectedCamera.CreatedOn, actualCamera.CreatedOn);
            Assert.Equal(expectedCamera.StorageId, actualCamera.StorageId);
        }

        private CameraStorage CreateStorageItem()
        {
            CameraStorage result;

            using (var context = new CameraDatabaseContext(dbContextOptions))
            {
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();

                var dbSetCamera = context.Set<Camera>();

                var camera = new Camera("Living room");                

                dbSetCamera.Add(camera);

                var dbSetStorageItem = context.Set<CameraStorage>();

                var storageItem = new CameraStorage(camera, "storage id");

                dbSetStorageItem.Add(storageItem);

                context.SaveChanges();

                result = storageItem;
            }

            return result;
        }

        private IRepository<CameraStorage> GetRepository()
        {
            var dbContextResolver = new DbContextResolver(new CameraDatabaseContext(dbContextOptions));

            var repository = new Repository<CameraStorage>(dbContextResolver);

            return repository;
        }
    }
}
