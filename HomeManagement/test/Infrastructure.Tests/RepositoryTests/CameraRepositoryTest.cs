﻿using Infrastructure.DatabaseContexts;
using Infrastructure.Repositories;
using Xunit;
using Domain.Models.HomeAutomation;
using SharedKernel.Interfaces;

namespace Infrastructure.Tests.RepositoryTests
{
    [Collection("DatabaseTest")]
    public class CameraRepositoryTest : RepositoryTest
    {        
        public CameraRepositoryTest() : base()
        {
        }

        [Fact]
        public void AddTest()
        {
            var expectedCamera = CreateCamera();

            var cameraRepository = GetRepository();

            var actualCamera = cameraRepository.Get(expectedCamera.Id);

            Assert.Equal(expectedCamera.Id, actualCamera.Id);
            Assert.Equal(expectedCamera.EnableMotionDetection, actualCamera.EnableMotionDetection);
        }

        [Fact]
        public async Task UpdateTest()
        {
            var expectedCamera = CreateCamera();

            expectedCamera.DisableMotionDetection();

            var cameraRepository = GetRepository();

            cameraRepository.Update(expectedCamera);

            await cameraRepository.SaveChangesAsync();

            var actualCamera = cameraRepository.Get(expectedCamera.Id);

            Assert.Equal(expectedCamera.Id, actualCamera.Id);
            Assert.False(actualCamera.MotionDetection);
        }

        private Camera CreateCamera()
        {
            Camera result;

            using (var context = new CameraDatabaseContext(dbContextOptions))
            {
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();

                var dbSet = context.Set<Camera>();

                var camera = new Camera("Living room");

                camera.EnableMotionDetection();

                dbSet.Add(camera);

                context.SaveChanges();

                result = camera;
            }

            return result;
        }

        private IRepository<Camera> GetRepository()
        {
            var dbContextResolver = new DbContextResolver(new CameraDatabaseContext(dbContextOptions));

            var repository = new Repository<Camera>(dbContextResolver);

            return repository;
        }
    }
}
