﻿using Infrastructure.DatabaseContexts;
using Infrastructure.Tests.Setup;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Tests
{
    public abstract class RepositoryTest
    {
        protected readonly DbContextOptions<CameraDatabaseContext> dbContextOptions;

        protected RepositoryTest()
        {
            dbContextOptions = new DbContextOptionsBuilder<CameraDatabaseContext>()
                    .UseSqlServer(ConfigurationManager.SqlConnectionString)
                    .Options;
        }
    }
}
