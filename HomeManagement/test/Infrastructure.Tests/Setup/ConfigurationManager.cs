﻿using Microsoft.Extensions.Configuration;

namespace Infrastructure.Tests.Setup
{
    internal class ConfigurationManager
    {
        private static IConfigurationRoot config;

        static ConfigurationManager()
        {
            config = new ConfigurationBuilder()
                    .AddJsonFile("appsettings.json")
                    .Build();
        }

        public static string SqlConnectionString { 
            get {
                return config.GetConnectionString("DefaultConnection");
            } 
        }
    }
}
