﻿using System;
using System.IO;
using System.Reflection;

namespace UseCases.Tests.Utils
{
    internal class TestFiles
    {
        public static FileInfo GetDefaultImage()
        {
            string codeBase = Assembly.GetExecutingAssembly().Location;
            ArgumentNullException.ThrowIfNull(codeBase);

            var assemblyPath = Path.GetDirectoryName(new Uri(codeBase).LocalPath);
            ArgumentNullException.ThrowIfNull(assemblyPath);

            var filePath = Path.Combine(assemblyPath, "SampleTestFiles");

            var fileInfo = new FileInfo(Path.Combine(filePath, "test_image.jpg"));

            return fileInfo;
        }
    }
}
