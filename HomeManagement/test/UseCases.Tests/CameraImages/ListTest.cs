﻿using UseCases.Interfaces;
using Moq;
using UseCases.CameraImages;
using Xunit;
using UseCases.CameraImages.Repository.ListCameraImages.Dtos.Requests;
using UseCases.CameraImages.List.Repository.ListCameraImages;
using UseCases.CameraImages.List.Repository.ListCameraImages.Dtos.Responses;
using UseCases.CameraImages.List.Dtos.Responses;
using UseCases.CameraImages.List;

namespace UseCases.Tests.CameraImages
{
    public class ListTest
    {
        private Mock<IListCameraStorageRepository> repositoryMock;
        private Mock<IStorage> storageMock;

        private CameraImagesListItemResponse expectedResponseFirst;
        private CameraImagesListItemResponse expectedResponseLast;

        public ListTest()
        {
            repositoryMock = new Mock<IListCameraStorageRepository>();
            storageMock = new Mock<IStorage>();
        }

        [Fact]
        public void Get()
        {
            expectedResponseFirst = new CameraImagesListItemResponse()
            {
                Id = 1,
                Thumbnail = new Uri("http://www.example.com/somethumbnails/863345"),
                CreatedOn = DateTime.UtcNow.AddDays(-6)
            };

            expectedResponseLast = new CameraImagesListItemResponse()
            {
                Id = 2,
                Thumbnail = new Uri("http://www.example.com/somethumbnails/876223"),
                CreatedOn = DateTime.UtcNow.AddDays(-5)
            };

            var expectedResult = new CameraImagesListResponse();
            expectedResult.Items = new List<CameraImagesListItemResponse>()
            {
                expectedResponseFirst,
                expectedResponseLast
            };

            var useCaseRequest = new ListCameraImagesUseCaseRequest()
            {
                PageNumber = 2,
                PageSize = 60
            };

            var responseMock = RepositoryMock(useCaseRequest);
            StorageMock(responseMock);

            var givenUseCase = new CameraImagesListUseCase(repositoryMock.Object, storageMock.Object);
            var givenResult = givenUseCase.Get(useCaseRequest);

            Assert.Equal(2, givenResult.Items.Count());
            AssertItems(expectedResponseFirst, givenResult.Items.First());
            AssertItems(expectedResponseLast, givenResult.Items.Last());
        }

        private void StorageMock(IEnumerable<ListCameraImagesItemRepositoryResponse> responseMock)
        {            
            storageMock
                .Setup(
                    x => x.GetUri(It.Is<IEnumerable<string>>(x => 
                        x.Contains(responseMock.First().StorageId)
                        && x.Contains(responseMock.Last().StorageId)
                    )))
                .Returns(
                    new[] {
                        expectedResponseFirst.Thumbnail,
                        expectedResponseLast.Thumbnail});
        }

        private IEnumerable<ListCameraImagesItemRepositoryResponse> RepositoryMock(
            ListCameraImagesUseCaseRequest useCaseRequest)
        {
            var repositoryResponse = new[] {
                new ListCameraImagesItemRepositoryResponse()
                {
                    Id = expectedResponseFirst.Id,
                    CreatedOn = expectedResponseFirst.CreatedOn,
                    StorageId = $"SomeId{expectedResponseFirst.Id}"
                },
                new ListCameraImagesItemRepositoryResponse()
                {
                    Id = expectedResponseLast.Id,
                    CreatedOn = expectedResponseLast.CreatedOn,
                    StorageId = $"SomeId{expectedResponseLast.Id}"
                }
            };

            repositoryMock
                .Setup(x => x.List(It.Is<ListCameraImagesRepositoryRequest>(x =>
                            x.PageNumber == useCaseRequest.PageNumber &&
                            x.PageSize == useCaseRequest.PageSize)))
                .Returns( repositoryResponse );

            return repositoryResponse;
        }

        private void AssertItems(CameraImagesListItemResponse expected, CameraImagesListItemResponse given)
        {
            Assert.Equal(expected.Thumbnail, given.Thumbnail);
            Assert.Equal(expected.CreatedOn, given.CreatedOn);
        }
    }
}
