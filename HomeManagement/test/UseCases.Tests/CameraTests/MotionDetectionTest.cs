﻿using Domain.Models.HomeAutomation;
using Moq;
using SharedKernel.Interfaces;
using UseCases.CameraImages.Repository;
using UseCases.Cameras.MotionDetection;
using Xunit;

namespace UseCases.Tests.CameraTests
{
    public class MotionDetectionTest
    {
        [Fact]
        public async Task Set()
        {
            var camera = new Camera("Living room camera");
            
            var repositoryMock = new Mock<IRepository<Camera>>();

            repositoryMock
                .Setup(x => x.Get(camera.Id))
                .Returns(camera);

            repositoryMock
                .Setup(
                    x => x.Update(
                        It.Is<Camera>(x => x.MotionDetection == true)));

            var useCase = new CameraMotionDetectionUseCase(repositoryMock.Object);
            
            await useCase.Set(camera.Id);
            
            repositoryMock.VerifyAll();
        }
    }
}
