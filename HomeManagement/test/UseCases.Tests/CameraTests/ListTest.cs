﻿using Moq;
using Xunit;
using Domain.Models.HomeAutomation;
using UseCases.Cameras.List;
using UseCases.CameraImages.Repository;
using SharedKernel.Interfaces;

namespace UseCases.Tests.CameraTests
{
    public class ListTest
    {
        [Fact]
        public async Task Get()
        {
            var expectedCameraLivingRoom = new Camera("Camera living room");
            var expectedCameraGarden = new Camera("Camera garden");

            var useCaseRequest = new ListCameraUseCaseRequest()
            {
                PageNumber = 2,
                PageSize = 10
            };

            var repositoryMock = new Mock<IRepository<Camera>>();

            repositoryMock
                .Setup(x => x.ListReadOnlyAsync(It.Is<ListCameraUseCaseRequest>(x =>
                            x.PageNumber == useCaseRequest.PageNumber &&
                            x.PageSize == useCaseRequest.PageSize)))
                .ReturnsAsync(new List<Camera>() { expectedCameraLivingRoom, expectedCameraGarden });

            var givenUseCase = new ListCamerasUseCase(repositoryMock.Object);
            var givenResult = await givenUseCase.Get(useCaseRequest);

            Assert.Equal(2, givenResult.Items.Count());
            AssertItems(expectedCameraLivingRoom, givenResult.Items.First());
            AssertItems(expectedCameraGarden, givenResult.Items.Last());
        }

        private void AssertItems(Camera expected, CamerasListItemResponse given)
        {
            Assert.Equal(expected.Id, given.Id);
            Assert.Equal(expected.Name, given.Name);
            Assert.Equal(expected.MotionDetection, given.EnableMotionDetection);
        }
    }
}
