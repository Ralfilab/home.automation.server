﻿using Moq;
using SharedKernel.Interfaces;
using UseCases.Cameras.Add.Dtos;
using Xunit;
using Domain.Models.HomeAutomation;
using UseCases.Cameras.Add;

namespace UseCases.Tests.CameraTests
{
    public class AddCameraTest
    {
        [Fact]
        public async Task SuccessfullyAddCamera()
        {
            var expectedCamera = new Camera("Living room camera");
            
            var repositoryMock = new Mock<IRepository<Camera>>();

            repositoryMock
                .Setup(x => x.AddAsync(
                    It.Is<Camera>(x => x.Name == expectedCamera.Name)
                    ))
                .ReturnsAsync(expectedCamera);

            var useCase = new CameraAddUseCase(repositoryMock.Object);

            var givenDto = new CameraAddRequest() 
            { 
                Name = expectedCamera.Name
            };

            var givenResult = await useCase.Add(givenDto);

            repositoryMock.VerifyAll();

            Assert.Equal(expectedCamera.Id, givenResult.Id);
            Assert.Equal(expectedCamera.Name, givenResult.Name);
            Assert.Equal(expectedCamera.ApiKey.Key, givenResult.ApiKey);
        }
    }
}
