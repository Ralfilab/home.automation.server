++++++++++++++++++++ NAMING CONVENTION ++++++++++++++++++++

Controller
 - {ResourceName}{Action}Controller

DTOs for API communicatation
 - {ResourceName}{Action}ApiRequest
 - {ResourceName}{Action}ApiResponse
 
Use Cases
 - {ResourceName}{Action}UseCase
 
DTOs for use cases
 - {ResourceName}{Action}Request
 - {ResourceName}{Action}Response
 
 Handlers
 - {ResourceName}{Action}Handler


Unit test
 - install https://dotnet.microsoft.com/en-us/download/dotnet/thank-you/runtime-3.1.28-windows-x64-installer

++++++++++++++++++++ Migrations ++++++++++++++++++++

How to install dotnet ef tool?


Migration run inside the Infrastructure project:
- Run migration:
	dotnet ef database update 
	
- Generate sql script
	dotnet ef migrations script

Add a new migration inside Infrastructure (.\Infrastructure\) project. This will also automatically add / update entities.
- Add new migration (run from src/Infrastructure)
	dotnet ef migrations add DefaultAdminUser
	

++++++++++++++++++++ DOCKER ++++++++++++++++++++
Docker build image locally!
First make sure the docker file is in the same folder as sln file. It can be copied over from Api file,
as that's where VS put it in the first place. Make sure it's named correctly.
docker build ./ -t ralfilab/homeautomation.api:0.0.4

Docker deploy to Docker Hub
docker push ralfilab/homeautomation.api:0.0.4

++++++++++++++++++++ KUBERNETES INGRESS CONTROLLER INSTALL ++++++++++++++++++++ 
https://learn.microsoft.com/en-us/azure/aks/ingress-tls?tabs=azure-cli

- install ingress-nginx
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.7.0/deploy/static/provider/cloud/deploy.yaml

- check if all ok
kubectl get pods --namespace ingress-nginx

- browser check, hit the public ip adress. It should should nginx 404 error.

JUST IN CASE SOMETHING WENT WRONG, HERE IS HOW TO DELETE IT!!!
kubectl delete namespace ingress-nginx

- FQDN - azure dns name
- run this to get resource name:
az network public-ip list --query "[?ipAddress!=null]|[?contains(ipAddress, 'ReplaceWithPublicIpAddressOfIngress')].[id]" --output tsv

az network public-ip update --ids 'ReplaceWithResourceName' --dns-name 'ralfilab-home-automation'

- current dns:
http://ralfilab-home-automation.northeurope.cloudapp.azure.com/

- install cert manager
kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.11.1/cert-manager.yaml

++++++++++++++++++++ KUBERNETES ++++++++++++++++++++
kubectl apply -f ./aks-homeautomation.api.yaml


Troubleshooking - get error message from pod:
kubectl logs --previous --tail 10 -p [PODNAME] --previous=false

Shell to pod
kubectl exec -it {PODNAME} -- bash


Certificate for Docker dev:
https://github.com/dotnet/AspNetCore.Docs/issues/6199
https://learn.microsoft.com/en-us/aspnet/core/security/docker-compose-https?view=aspnetcore-7.0

Create database password as an env variable:
kubectl create secret generic homemanagementapi-db --from-literal=connectionstringdb='Server=tcp:homeautomationsql.database.windows.net,1433;Initial Catalog=HomeAutomationInfrastructure;Persist Security Info=False;User ID=sqlHomeAutomationUser;Password={your_password};MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;'

Create azure storage connection string as an env variable:
kubectl create secret generic homemanagementapi-storage --from-literal=connectionstringstorage='{connectionString}'

kubectl create secret generic homeautomationapi-bus --from-literal=connectionstringbus='{connectionString}'

++++++++++++++++++++ DOCKER ++++++++++++++++++++
docker run -it --rm --name rabbitmq -p 5672:5672 -p 15672:15672 rabbitmq:3.11-management
docker run -p 10000:10000 -p 10001:10001 -p 10002:10002 mcr.microsoft.com/azure-storage/azurite


