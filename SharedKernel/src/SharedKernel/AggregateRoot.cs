﻿using SharedKernel.Interfaces;

namespace SharedKernel
{
    public abstract class AggregateRoot : Entity
    {
        private readonly List<DomainEvent> domainEvents = new List<DomainEvent>();

        public virtual IReadOnlyList<DomainEvent> DomainEvents => domainEvents;

        protected AggregateRoot()
        {
        }

        protected AggregateRoot(long id)
            : this()
        {
            Id = id;
        }

        protected virtual void AddDomainEvent(DomainEvent newEvent)
        {
            domainEvents.Add(newEvent);
        }

        public virtual void ClearEvents()
        {
            domainEvents.Clear();
        }
    }
}
