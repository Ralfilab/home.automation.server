﻿namespace UseCases.Interfaces.Storage.Dtos
{
    public class StorageAddRequest
    {
        public string FileExtension { get; set; }
        public byte[] FileBytes { get; set; }
    }
}
