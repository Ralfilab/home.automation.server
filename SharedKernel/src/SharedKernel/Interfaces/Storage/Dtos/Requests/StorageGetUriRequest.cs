﻿namespace UseCases.Interfaces.Storage.Dtos
{
    public class StorageGetUriRequest
    {
        public IEnumerable<string> Ids { get; set; }
    }
}
