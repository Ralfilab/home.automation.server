﻿using UseCases.Interfaces.Storage.Dtos;

namespace UseCases.Interfaces
{
    public interface IStorage
    {
        Task<string> AddAsync(StorageAddRequest dto);

        IEnumerable<Uri> GetUri(IEnumerable<string> locations);
    }
}