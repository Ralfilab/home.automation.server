﻿using SharedKernel;
using SharedKernel.Interfaces;
using System.Linq.Expressions;

namespace SharedKernel.Interfaces
{
    public interface IRepository<TEntity>
        where TEntity : Entity
    {
        TEntity Get(long id);

        TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate);

        /// TODO: Remove!!!! No read only removing whole entities
        Task<IEnumerable<TEntity>> ListReadOnlyAsync(IPaging paging);

        void Update(TEntity entity);

        TEntity Add(TEntity entity);

        Task<TEntity> AddAsync(TEntity entity);

        Task<int> CountAsync(Expression<Func<TEntity, bool>> predicate);

        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
    }
}
