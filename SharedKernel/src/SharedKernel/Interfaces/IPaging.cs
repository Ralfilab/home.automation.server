﻿namespace SharedKernel.Interfaces
{
    public interface IPaging
    {
        int PageNumber { get; set; }

        int PageSize { get; set; }
    }
}
