﻿namespace SharedKernel.Events
{
    public class CameraMotionDetectionEnabled
    {
        public long CameraId { get; set; }
    }
}
