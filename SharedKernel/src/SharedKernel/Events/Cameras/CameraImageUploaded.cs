﻿namespace SharedKernel.Events.Cameras
{
    public record CameraImageUploaded
    {
        public string ApiKey { get; init; }

        public string StorageId { get; init; }
    }
}
