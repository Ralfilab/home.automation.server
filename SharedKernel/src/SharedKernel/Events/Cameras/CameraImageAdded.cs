﻿namespace SharedKernel.Events.Cameras
{
    public record CameraImageAdded
    {
        public long CameraImageId { get; init; }        
    }
}
