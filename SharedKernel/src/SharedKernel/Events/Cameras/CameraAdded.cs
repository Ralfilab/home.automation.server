﻿namespace SharedKernel.Events
{
    public record CameraAdded
    {
        public long CameraId { get; init; }

        public string ApiKey { get; init; }
    }
}
