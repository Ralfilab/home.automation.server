﻿namespace SharedKernel.Events
{
    public class CameraMotionDetectionDisabled
    {
        public long CameraId { get; set; }
    }
}
