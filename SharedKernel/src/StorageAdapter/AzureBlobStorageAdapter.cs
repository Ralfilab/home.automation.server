﻿using Azure.Storage.Blobs;
using Azure.Storage.Sas;
using UseCases.Interfaces;
using UseCases.Interfaces.Storage.Dtos;

namespace StorageAdapter
{
    public class AzureBlobStorageAdapter : IStorage
    {
        private static readonly string containerName = "my-home";

        public async Task<string> AddAsync(StorageAddRequest dto)
        {            
            string? connectionString = Environment.GetEnvironmentVariable("AzureStorageConnectionString");

            ArgumentNullException.ThrowIfNull(connectionString);

            var id = Guid.NewGuid().ToString() + dto.FileExtension;                        
                        
            var blobContainerClient = new BlobContainerClient(connectionString, containerName);
            
            var blobClient = blobContainerClient.GetBlobClient(id);
            await blobClient.UploadAsync(new BinaryData(dto.FileBytes));

            return id;
        }

        public IEnumerable<Uri> GetUri(IEnumerable<string> locations)
        {
            string? connectionString = Environment.GetEnvironmentVariable("AzureStorageConnectionString");
                        
            var result = new List<Uri>();
            var blobSasBuilder = new BlobSasBuilder(BlobContainerSasPermissions.Read, DateTime.UtcNow.AddMinutes(5))
            {
                BlobContainerName = containerName                
            };
            
            var blobContainerClient = new BlobContainerClient(connectionString, containerName);            

            foreach (var location in locations)
            {
                var blobUriWithSas = blobContainerClient
                    .GetBlobClient(location)
                    .GenerateSasUri(blobSasBuilder); 
                
                result.Add(blobUriWithSas);
            }

            return result;         
        }
    }
}
