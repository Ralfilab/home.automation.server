﻿using UseCases.Interfaces;
using UseCases.Interfaces.Storage.Dtos;

namespace DiskStorageAdapter
{
    public class DiskStorage : IStorage
    {
        public async Task<string> AddAsync(StorageAddRequest dto)
        {            
            var id = Guid.NewGuid().ToString() + dto.FileExtension;

            var path = Path.Combine("C:/SomePath", id);

            await File.WriteAllBytesAsync(path, dto.FileBytes);

            return id;
        }

        public IEnumerable<Uri> GetUri(IEnumerable<string> locations)
        {
            throw new NotImplementedException();
        }
    }
}
